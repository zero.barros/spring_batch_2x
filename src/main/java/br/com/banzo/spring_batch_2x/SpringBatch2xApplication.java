package br.com.banzo.spring_batch_2x;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBatch2xApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBatch2xApplication.class, args);
    }

}
